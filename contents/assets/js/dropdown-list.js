function dropdownClickHandler(event) {
  let dropdownHeader = event.target;
  let dropdown = dropdownHeader.parentElement;
  let downIcon = dropdownHeader.children[1];
  let upIcon = dropdownHeader.children[2];

  dropdown.classList.toggle('open');

  upIcon.classList.toggle('active');
  downIcon.classList.toggle('active');
}

