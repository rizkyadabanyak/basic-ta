<style>
    .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
        color: #fff;
        background-color: #F2C808;
        border-color: #F2C808; /*set the color you want here*/
    }
</style>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

<div class="container" style="margin-top: 50px;margin-bottom: 50px">
    <div class="row">
        <div class="col-md-6">
            <center>
                <button class="btn btn-primary bg-color-prmy m-3 p-3" style="font-size: 30px;border-radius: 20px">TAMBAH AJUAN</button>
            </center>
        </div>
        <div class="col-md-6">
            <center>
                <button class="btn btn-primary bg-color-prmy m-3 p-3"  style="font-size: 30px;border-radius: 20px">HISTORY AJUAN</button>
            </center>
        </div>
    </div>
    <br><br>
    <a class="mb-1" style="font-size: 30px!important;color: #0B2C58 !important;font-weight: bold;border-bottom: 6px #FFC107 solid;font-size: 20px;">History Pengajuan Dana </a>
    <br><br><br><br>
    <div>
        <canvas id="myChart"></canvas>
    </div>

    <div>
        <table id="table_id" class="display">
            <thead>
            <tr>
                <th>Column 1</th>
                <th>Column 2</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Row 1 Data 1</td>
                <td>Row 1 Data 2</td>
            </tr>
            <tr>
                <td>Row 2 Data 1</td>
                <td>Row 2 Data 2</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>



<script>

</script>

<script>

    const labels = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
    ];

    const data = {
        labels: labels,
        datasets: [{
            label: 'My First dataset',
            backgroundColor: 'rgb(234,170,38)',
            borderColor: 'rgb(234,170,38)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {}
    };

    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );

    const table = new DataTable('#table_id', {
        // options
    });


</script>