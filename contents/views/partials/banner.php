<div class="main-background-gradient pt-4">
    <div class="hero-section  py-3 container d-flex min-vh-100">
        <div class="pt-4 ">
	  <h1 class="mb-1">Sipenda.</h1>
	  <p class="subtitle">Manfaatkan pengajuan dana untuk kegiatan ormawa</p>
	  <p>
	      SIpenda adalah website pengajuan dana untuk organisasi mahasiswa politeknik elektronika negeri surabaya (PENS). Sipenda bertujuan untuk mempermudah dan mempercepat pengajuan dana organisasi mahasiswa politeknik elektronika negeri surabaya
	  </p>
	  <div class="countdown d-flex my-5 text-center">
	      <div class="countdown__time countdown__time--days">
		<p>18</p>
		<small>Days</small>
	      </div>
	      <p class="countdown__separator">:</p>
	      <div class="countdown__time countdown__time--hours">
		<p>23</p>
		<small>Hours</small>
	      </div>
	      <p class="countdown__separator">:</p>
	      <div class="countdown__time countdown__time--minutes">
		<p>56</p>
		<small>Minutes</small>
	      </div>
	      <p class="countdown__separator">:</p>
	      <div class="countdown__time countdown__time--seconds">
		<p>43</p>
		<small>Second</small>
	      </div>
	  </div>
	  <div class="call-to-action d-flex">
	      <a href="../index.php" class="d-flex align-items-center register-btn mr-2">
		Daftar Sekarang
		<svg class="ml-2" width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
		    <path fill-rule="evenodd" clip-rule="evenodd" d="M14 25.6667C20.4433 25.6667 25.6667 20.4433 25.6667 14C25.6667 7.55668 20.4433 2.33333 14 2.33333C7.55668 2.33333 2.33333 7.55668 2.33333 14C2.33333 20.4433 7.55668 25.6667 14 25.6667ZM14 28C21.732 28 28 21.732 28 14C28 6.26801 21.732 0 14 0C6.26801 0 0 6.26801 0 14C0 21.732 6.26801 28 14 28Z" fill="white"/>
		    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.7758 9.10189C12.282 8.63277 11.4931 8.63263 10.9993 9.1019L12.7758 9.10189ZM12.7758 9.10189L17 13.115C17.2514 13.354 17.3829 13.6746 17.3829 13.9999C17.3829 14.3254 17.2513 14.646 16.9999 14.8849L12.7758 18.8979C12.2821 19.3671 11.493 19.3674 10.9992 18.898C10.4886 18.4129 10.4885 17.6131 10.9992 17.128L14.2918 13.9999L10.9993 10.872C10.4889 10.3868 10.4887 9.58711 10.9993 9.1019" fill="white"/>
		</svg>
	      </a>
	      <a href="https://himitpens.com/panduan-it-creation" target="_blank" class="d-flex align-items-center transparent-btn">
		Pedoman
		<svg class="ml-2" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
		    <path d="M13.5 0C6.03793 0 0 6.03846 0 13.5C0 20.9621 6.03846 27 13.5 27C20.9621 27 27 20.9615 27 13.5C27 6.03793 20.9615 0 13.5 0ZM13.5 24.8906C7.20389 24.8906 2.10938 19.7957 2.10938 13.5C2.10938 7.20389 7.20431 2.10938 13.5 2.10938C19.7961 2.10938 24.8906 7.20431 24.8906 13.5C24.8906 19.7961 19.7956 24.8906 13.5 24.8906Z" fill="white"/>
		    <path d="M13.5 11.3026C12.9175 11.3026 12.4453 11.7747 12.4453 12.3572V19.1491C12.4453 19.7316 12.9175 20.2038 13.5 20.2038C14.0825 20.2038 14.5547 19.7315 14.5547 19.149V12.3572C14.5547 11.7747 14.0825 11.3026 13.5 11.3026Z" fill="white"/>
		    <path d="M13.5 10.0111C14.2864 10.0111 14.9238 9.37364 14.9238 8.58728C14.9238 7.80092 14.2864 7.16345 13.5 7.16345C12.7136 7.16345 12.0762 7.80092 12.0762 8.58728C12.0762 9.37364 12.7136 10.0111 13.5 10.0111Z" fill="white"/>
		</svg>
	      </a>
	  </div>
        </div>
        <div class="flex-shrink-0">
	  <img class="w-100 text-center" src="<?= base_url; ?>/assets/images/depan.png" alt="illustration">
        </div>
    </div>

</div>