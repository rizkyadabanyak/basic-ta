    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <div class="row">
    <div class="col-sm-12">
      <?php
        Flasher::Message();
      ?>
    </div>
  </div>
                <h2 class="section-title">Berita</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="<?= base_url; ?>/AdminFaqs/create" class="btn btn-success text-white">create</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped1" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Pertanyaan</th>
                                                <th>Jawaban</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                oci_execute($data['datas']);
                                                while($rows=oci_fetch_object($data['datas'])){
                                            ?>
                                            <tr>
                                                <td><?php echo $rows->PERTANYAAN; ?></td>
                                                <td><?php echo $rows->JAWABAN; ?></td>
                                                <td>
                                                
                                                    <a href="<?= base_url; ?>/AdminFaqs/edit/<?= $rows->ID_FAQ ?>" class="btn btn-primary text-white">Edit</a>
                                                    <a href="<?= base_url; ?>/AdminFaqs/destroy/<?=$rows->ID_FAQ ?>" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                                </td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    
 <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>