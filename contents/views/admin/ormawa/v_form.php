<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form Ormawa</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                if ( $data['cek_form'] == 'edit'){
                                    $hasil = $data['news'];
                                    oci_execute($hasil) or die(oci_error());				
	                                $baris = oci_fetch_object($hasil);
                                }
                                ?>
                                
                                <form action="<?= ($data['cek_form'] == 'edit') ? base_url.'/AdminOrmawa/update/'.$baris->ID_ORMAWA :  base_url.'/AdminOrmawa/store' ?>" method="POST" enctype="multipart/form-data">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="nama" id="nama" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->NAMA :  '' ?>">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="email" name="email" id="email" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->EMAIL :  '' ?>">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="password" name="password" id="password" class="form-control" value="">
                                            
                                        </div>
                                    </div>
                                
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>