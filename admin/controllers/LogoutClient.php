<?php

class LogoutClient {
	public function LogoutClient(){
		session_start();
		unset($_SESSION['session_login_client']);
		unset($_SESSION['emailClient']);
		unset($_SESSION['nameClient']);
		unset($_SESSION['nomorClient']);

		header('location: '. base_url . '/LoginClient');
	}

}