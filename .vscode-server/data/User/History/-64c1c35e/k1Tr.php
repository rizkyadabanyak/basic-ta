<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Form FAQS</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <?php
                            if ($data['cek_form'] == 'edit') {
                                $hasil = $data['news'];
                                oci_execute($hasil) or die(oci_error());
                                $baris = oci_fetch_object($hasil);
                            }
                            ?>

                            <form action="<?= ($data['cek_form'] == 'edit') ? base_url . '/AdminFaqs/update/' . $baris->ID_FAQ :  base_url . '/AdminFaqs/store' ?>" method="POST" enctype="multipart/form-data">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pertanyaan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control select2">
                                            <option>Option 1</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jawaban</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="jawaban" id="judul" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->JAWABAN :  '' ?>">

                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
</script>