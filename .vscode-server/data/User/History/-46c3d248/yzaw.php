<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form News</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                if ( $data['cek_form'] == 'edit'){
                                    $hasil = $data['news'];
                                    oci_execute($hasil) or die(oci_error());				
	                                $baris = oci_fetch_object($hasil);
                                }
                            
                                ?>
                                <form action="<?= base_url; ?>/AdminNews/store" method="POST" enctype="multipart/form-data">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">judul berita</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="judul" id="judul" class="form-control" value="<?php () echo $baris->JUDUL; ?>">
                                            
                                        </div>
                                    </div>
                                
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">thumbnail</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">deskripsi</label>
                                        <div class="col-sm-12 col-md-7">    
                                            <textarea id="summernote" name="dsc"><?php echo $baris->DSC; ?></textarea>
                                       
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>