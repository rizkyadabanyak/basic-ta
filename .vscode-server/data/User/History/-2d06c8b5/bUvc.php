<?php

class LoginClient extends Controller {

	public function __construct()
	{
		// if($_SESSION['session_login_client'] == 'true') {
		// 	header('location: '. base_url . '/Admin');
		// 	exit;
		// }
	}
	
	public function index()
	{

	    $data['title'] = 'Halaman Login';
	    $data['aku']  = "aku";
		$data['cek'] = 'Mahasiswa';
	    $this->view('views/partials/header',$data);
//	    $this->view('views/partials/banner',$data);
	    $this->view('views/client/auth/login',$data);
	    $this->view('views/partials/footer');
	}

	public function prosesLogin() {
		// echo $_POST['email'];
		
		$u= $_POST['email'];
        $p =$_POST['password'];


        $header=array("netid: $u","password: ".base64_encode($p));
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($data, CURLOPT_HTTPHEADER, $header);
        curl_setopt($data, CURLOPT_URL, "https://login.pens.ac.id/auth/");
        curl_setopt($data, CURLOPT_TIMEOUT, 9);

        $hasil = curl_exec($data);
        curl_close($data);

		$decode = json_decode($hasil);
		
		if($hasil != 'auth error'){
			
			if($decode->NRP){
				$_SESSION['email'] = $u;
				$_SESSION['name'] =  $decode->Name;
				$_SESSION['nomor'] = $decode->NRP;
				$_SESSION['session_login'] = 'true';

				header('location: '. base_url . '/ClientDashboard');
			}else{
				
				Flasher::setMessage('Username / Password','salah.','danger');
				header('location: '. base_url . '/LoginClient');
			}
			
		}else{
			
			Flasher::setMessage('Username / Password','salah.','danger');
			header('location: '. base_url . '/LoginClient');
		}
		//   echo $decode->Name;

		
        // echo $decode->NRP;


		// if($row = $this->model('LoginModel')->checkLogin($_POST) > 0 ) {
		// 		$_SESSION['username'] = $row['username'];
		// 		$_SESSION['nama'] = $row['nama'];
		// 		$_SESSION['session_login'] = 'sudah_login';

		// 		//$this->model('LoginModel')->isLoggedIn($_SESSION['session_login']);

		// 		header('location: '. base_url . '/home');
		// } else {
		// 	Flasher::setMessage('Username / Password','salah.','danger');
		// 	header('location: '. base_url . '/login');
		// 	exit;
		// }
	}
}