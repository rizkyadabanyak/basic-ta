    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Berita</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.berita.create')}}" class="btn btn-success text-white">create</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table class=" table table-striped" id="item">
                                        <thead>
                                            <tr>
                                                <th>Judul</th>
                                                <th>Deskripsi</th>
                                                <th>img</th>
                                                <th>Tanggal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?=
                                            oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
                                             foreach($rows as $hasil) {  ?>
                                            <tr>
                                                <td>$hasil['JUDUL']</td>
                                                <td>$hasil['JUDUL']</td>
                                                <td><img src="{{asset($berita->img)}}" width="50"></td>

                                                <td>$hasil['IMG']</td>
                                                <td>
                                                    <a href="{{route('admin.auth.berita.edit',$berita->id)}}" class="btn btn-primary text-white">Edit</a>
                                                    <a href="{{route('admin.auth.beritaDestroy',$berita->id)}}" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                                </td>
                                            </tr>
                                            <?=} ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
