    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Berita</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.berita.create')}}" class="btn btn-success text-white">create</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Judul</th>
                                                <th>Deskripsi</th>
                                                <th>img</th>
                                                <th>Tanggal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                oci_execute($data['news']);
                                                while($rows=oci_fetch_object($data['news'])){
                                            ?>
                                            <tr>
                                                <td><?php echo $rows->JUDUL; ?></td>
                                                <td><?php echo $base_url .$rows->IMG; ?></td>
                                                <td><img src="<?php base_url.$rows->IMG ?>" width="50"></td>

                                                <td><?php echo $rows->CREATED_AT; ?></td>
                                                <td>
                                                    <a href="{{route('admin.auth.berita.edit',$berita->id)}}" class="btn btn-primary text-white">Edit</a>
                                                    <a href="{{route('admin.auth.beritaDestroy',$berita->id)}}" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                                </td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 