    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Berita</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.berita.create')}}" class="btn btn-success text-white">create</a>
                                <br><br></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
