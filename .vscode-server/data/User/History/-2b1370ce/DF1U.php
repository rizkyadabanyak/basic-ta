    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Berita</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.berita.create')}}" class="btn btn-success text-white">create</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table class=" table table-striped" id="item">
                                        <thead>
                                            <tr>
                                                <th>Judul</th>
                                                <th>Deskripsi</th>
                                                <th>img</th>
                                                <th>Tanggal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            echo $data['news'];

                                            foreach ($rows as $data['news']) {
                                                echo "Nama : " . $data['JUDUL'] ." - Stok : " . $data['DSC'] . "<br>";
                                            }
                                            ?>
                                
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><img src="{{asset($berita->img)}}" width="50"></td>

                                                <td></td>
                                                <td>
                                                    <a href="{{route('admin.auth.berita.edit',$berita->id)}}" class="btn btn-primary text-white">Edit</a>
                                                    <a href="{{route('admin.auth.beritaDestroy',$berita->id)}}" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
