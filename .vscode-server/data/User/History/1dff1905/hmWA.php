<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Rizky Putra</a>
    </div>
    <div class="footer-right">
        2.3.0
    </div>
</footer>
</div>
</div>
<!-- JS Libraies -->
  <script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
  <script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
  <script src="https://demo.getstisla.com/assets/modules/jquery-ui/jquery-ui.min.js"></script>

 
<!-- General JS Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?= base_url; ?>/assets/stisla/js/stisla.js"></script>

<!-- JS Libraies -->
<!--<script src="../node_modules/simpleweather/jquery.simpleWeather.min.js"></script>-->
<!--<script src="../node_modules/chart.js/dist/Chart.min.js"></script>-->
<!--<script src="../node_modules/jqvmap/dist/jquery.vmap.min.js"></script>-->
<!--<script src="../node_modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>-->
<!--<script src="../node_modules/summernote/dist/summernote-bs4.js"></script>-->
<!--<script src="../node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script>-->

 <!-- include summernote css/js -->
    <script src="https://demo.getstisla.com/assets/modules/summernote/summernote-bs4.js"></script>

<!-- Template JS File -->
<script src="<?= base_url; ?>/assets/stisla/js/scripts.js"></script>
<script src="<?= base_url; ?>/assets/stisla/js/custom.js"></script>

<!-- Page Specific JS File -->
<script src="<?= base_url; ?>/assets/stisla/js/page/index-0.js"></script>
  
</body>

 <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
</html>
