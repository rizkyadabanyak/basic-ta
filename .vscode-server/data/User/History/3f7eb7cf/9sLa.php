<?php
require '../contents/assets/Carbon/autoload.php';

use Carbon\Carbon;
use Carbon\CarbonInterval;

class AdminDana extends Controller
{
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
		$data['title'] = 'Sipenda.';
		$data['aku']  = "aku";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "SELECT * FROM FAQS ORDER BY CREATED_AT";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}

		$data['datas'] = $hasil;

		$this->view('views/partials-admin/header', $data);
		$this->view('views/partials-admin/sidebar', $data);
		$this->view('views/admin/dana/v_index', $data);
		$this->view('views/partials-admin/footer');
	}

	//ini buat view create
	public function create()
	{

		$data['title'] = 'Sipenda.';
		$data['aku']  = "aku";
		$data['Name'] = $_SESSION['name'];
		$data['cek_form'] = 'create';

		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "SELECT * FROM ORMAWA  ";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);


		$this->view('views/partials-admin/header', $data);
		$this->view('views/partials-admin/sidebar', $data);
		$this->view('views/admin/dana/v_form', $data);
		$this->view('views/partials-admin/footer');
	}
	//ini buat action create
	public function store()
	{

		$dt = Carbon::now();

		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "INSERT INTO FAQS (ID_FAQ,PERTANYAAN,JAWABAN,CREATED_AT) VALUES(SEQ_FAQ.nextval, :v1, :v2,:v3)";


		$data = array(
			':v1' => $_POST['pertanyaan'],
			':v2' => $_POST['jawaban'],
			':v3' => $dt->toDateTimeString()
		);

		$hasil = query_insert($con, $sql, $data);

		// var_dump($hasil);
		// return;
		Flasher::setMessage('Berhasil', 'ditambah', 'success');

		header('location: ' . base_url . '/AdminFaqs');
	}

	//ini buat view edit
	public function edit($id)
	{


		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "SELECT * FROM FAQS WHERE ID_FAQ='$id'";
		// $data = array(':v1' => $id);

		$hasil = oci_parse($con, $sql);

		$data['news'] = $hasil;
		$data['cek_form'] = 'edit';

		$this->view('views/partials-admin/header', $data);
		$this->view('views/partials-admin/sidebar', $data);
		$this->view('views/admin/faqs/v_form', $data);
		$this->view('views/partials-admin/footer');
		// var_dump($baris);
	}

	//ini buat action edit
	public function update($id)
	{

		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sqlData = "SELECT * FROM FAQs WHERE ID_FAQ='$id'";

		$getData = oci_parse($con, $sqlData);

		oci_execute($getData) or die(oci_error());
		$hasilGetData = oci_fetch_object($getData);

		$sql = "UPDATE FAQS SET PERTANYAAN=:v2,JAWABAN=:v3 WHERE ID_FAQ=:v1";


		$data = array(
			':v1' => $id,
			':v2' => $_POST['pertanyaan'],
			':v3' => $_POST['jawaban'],
		);

		$hasil = query_update($con, $sql, $data);
		Flasher::setMessage('Berhasil', 'update', 'success');

		header('location: ' . base_url . '/AdminFaqs');
	}

	//ini buat action delete
	public function destroy($id)
	{
		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "DELETE FAQS WHERE ID_FAQ=:v1";
		$data = array(':v1' => $id);
		$hasil = query_delete($con, $sql, $data);

		Flasher::setMessage('Berhasil', 'dihapus', 'success');

		header('location: ' . base_url . '/AdminFaqs');
		exit;
	}
}
