<style>
    body{
        background-color:#E5E5E5!important;
    }
    .card-besar {
        margin-top: 80px;
    }
    .card{
        width:700px;
        background-color:#FFFFFF;
    }
    .button-masuk{
        width:200px;
        background-color:#FFAE00;
        border-color: #FFAE00;
    }
    .button-masuk:hover{
        background-color:#1E3D59;
        border-color: #1E3D59;
    }
    .card-header {
        background-color: rgba(0,0,0,.03);
        border-bottom: 0px solid rgba(0,0,0,.125);
    }
    .card-footer {
        background-color: rgba(0,0,0,.03);
        border-top: 0px solid rgba(0,0,0,.125);
    }
    body{
        min-height:100vh;
    }
    .semua{
        height: 70vh;
        position:relative;
    }
</style>
<div class="semua">
    <div class="container ">
        <div class="card-besar d-flex justify-content-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center"><strong>L O G I N - <?= $data['cek']; ?></strong></h3>
                </div>

                <form action="<?= base_url; ?>/LoginAdmin/LoginOrmawa" method="POST">
                    <div class="card-body">
                        <div class="form-group" style="letter-spacing: 1px; color: #000000">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group" style="letter-spacing: 1px; color: #000000">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="card-footer">
                        <center>
                            <button type="submit" class="btn btn-primary btn-block button-masuk">M A S U K</button>
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>