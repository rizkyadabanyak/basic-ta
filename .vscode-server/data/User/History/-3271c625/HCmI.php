<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Form Ormawa</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <?php
                            if ($data['cek_form'] == 'edit') {
                                $hasil = $data['data'];
                                oci_execute($hasil) or die(oci_error());
                                $baris = oci_fetch_object($hasil);
                            }
                            ?>

                            <form action="<?= ($data['cek_form'] == 'edit') ? base_url . '/AdminOrmawa/validationForm/' . $baris->ID_ORMAWA :  base_url . '/AdminOrmawa/store' ?>" method="POST" enctype="multipart/form-data">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="nama" id="nama" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->NAMA :  '' ?>">

                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="email" name="email" id="email" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->EMAIL :  '' ?>" readonly="">

                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Singkatan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="singkatan" id="nama" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->SINGKATAN :  '' ?>">

                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Website</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="website" id="nama" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->WEBSITE :  '' ?>">

                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <center>
                                        <div class="selectgroup" style="width: 60% !important;">
                                            <label class="selectgroup-item">
                                                <input type="radio" name="cekOrmawa" value="hima" class="selectgroup-input" checked="">
                                                <span class="selectgroup-button">HIMA</span>
                                            </label>
                                            <label class="selectgroup-item">
                                                <input type="radio" name="cekOrmawa" value="ukm" class="selectgroup-input">
                                                <span class="selectgroup-button">UKM</span>
                                            </label>

                                        </div>
                                    </center>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Masukkan</label>
                                    <div id="ormawa">
                                        <div class="col-sm-12 col-md-12">
                                            <select class="selectpicker" multiple data-live-search="true" name="ormawa[]">
                                                <?php
                                                oci_execute($data['dataJurusan']);
                                                while ($rows = oci_fetch_object($data['dataJurusan'])) {
                                                ?>
                                                    <option value="<?php echo $rows->JURUSAN ?>"><?php echo $rows->JURUSAN ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="ukm" class="cocol-sm-12 col-md-7">
                                        <input type="text" name="namaUkm" id="nama" class="form-control" value="<?= ($data['cek_form'] == 'edit') ? $baris->UKM :  '' ?>" placeholder="masukkan nama UKM...">

                                    </div>
                                </div>


                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Logo Ormawa</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="logo_ormawa" id="image">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Logo Kabinet</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="logo_kabinet" id="image">
                                    </div>
                                </div>



                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function() {
        $('select').selectpicker();

        $('#summernote').summernote();

        $("div#ukm").hide();
        $("input[name=cekOrmawa]").on("click", function() {
            var selectedValue = $("input[name=cekOrmawa]:checked").val();
            if (selectedValue == "hima") {
                $("div#ormawa").show();
                $("div#ukm").hide();

            } else {
                if (selectedValue == "ukm") {
                    $("div#ormawa").hide();
                    $("div#ukm").show();
                }
            }

        });


    });
</script>