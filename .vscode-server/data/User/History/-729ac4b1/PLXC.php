<?php

class LogoutOrmawa {
	public function LogoutOrmawa(){
		session_start();
		unset($_SESSION['name']);
		unset($_SESSION['emailOrmawa']);
		unset($_SESSION['session_login']);
		unset($_SESSION['levelAdmin']);

		header('location: '. base_url . '/LoginClient');
	}

}