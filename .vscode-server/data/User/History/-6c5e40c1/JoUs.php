<?php

class Admin extends Controller {
	
	public function __construct()
	{
	}

	public function index()
	{
	    $data['title'] = 'Sipenda.';
	    $data['aku']  = "aku";
		$data['Name'] = $_SESSION['name'];
	    $this->view('views/partials-admin/header',$data);
	    $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/admin/v_dashboard',$data);
	    $this->view('views/partials-admin/footer');

	}
}