<?php

class Admin extends Controller {
	
	public function __construct()
	{
		if($_SESSION['session_login'] == 'true' && $_SESSION['levelAdmin'] == '1') {
			
		}else if($_SESSION['session_login'] == 'true' && $_SESSION['levelAdmin'] == '2'){
			header('location: '. base_url . '/AdminOrmawa');
			exit;
		}else{
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/LoginAdmin');
			exit;
		}
	}

	public function index()
	{
	    $data['title'] = 'Sipenda.';
	    $data['aku']  = "aku";
		$data['Name'] = $_SESSION['name'];
	    $this->view('views/partials-admin/header',$data);
	    $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/admin/v_dashboard',$data);
	    $this->view('views/partials-admin/footer');

	}
}