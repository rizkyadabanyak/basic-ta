    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <div class="row">
    <div class="col-sm-12">
      <?php
        Flasher::Message();
      ?>
    </div>
  </div>
                <h2 class="section-title">Ormawa</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="<?= base_url; ?>/AdminOrmawa/create" class="btn btn-success text-white">create</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped1" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Singkatan</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Jurusan</th>
                                                <th>Logo</th>
                                                <th>IMG_KABINET</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                oci_execute($data['datas']);
                                                while($rows=oci_fetch_object($data['datas'])){
                                            ?>
                                            <tr>
                                                <td><?php echo $rows->NAMA; ?></td>
                                                <td><?php echo $rows->SINGKATAN; ?></td>
                                                <td><?php echo $rows->EMAIL; ?></td>
                                                <td><?php echo $rows->WEBSITE; ?></td>
                                            
                                                <td><img src="<?= base_url.'/uploads/logo/'.$rows->LOGO ?>" width="50"></td>
                                                <td><img src="<?= base_url.'/uploads/kabinet/'.$rows->IMG_KABINET ?>" width="50"></td>

                                                <td><?php echo $rows->CREATED_AT; ?></td>
                                                <td>
                                                
                                                    <a href="<?= base_url; ?>/AdminNews/edit/<?= $rows->ID_PENGUMUMAN ?>" class="btn btn-primary text-white">Edit</a>
                                                    <a href="<?= base_url; ?>/AdminNews/destroy/<?=$rows->ID_PENGUMUMAN ?>" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                                </td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    
 <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>