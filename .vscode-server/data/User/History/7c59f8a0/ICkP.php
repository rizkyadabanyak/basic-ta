<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url; ?>/assets/apps.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <title><?= $data['title']; ?></title>
</head>
<style>
    .color-prmy {
        color: #103967;
    }
    .bg-color-prmy {
        background-color: #103967;
    }
</style>
<body>
<div class="main-background-gradient ">
<!--<div class="main-background-gradient min-vh-100">-->
    <header class="header navbar navbar-expand-xl navbar-dark py-3 container d-flex align-items-center" role="navigation">
        <a class="header__logo navbar-brand mr-4" href="<?= base_url; ?>">Sipenda.</a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
	  <nav class="navbar-nav align-items-center w-100">
	      <a class="nav-link mx-3" href="../index.php">Home</a>
	      <a class="nav-link mx-3" href="../index.php">Submit</a>
	      <a class="nav-link mx-3" href="../index.php">Announcement</a>
	      <a class="nav-link mx-3" href="/#faq">FAQ</a>

          <?php
            if(isset($_SESSION['session_login_client'])){

            ?> 

	      <div class="social-media d-flex align-items-center ml-auto">
            <a href="<?= base_url; ?>/LoginClient" class="d-flex align-items-center register-btn mr-2">
                <?= (isset($_SESSION['nameClient'])) ? '<i class="fa fa-user"></i> &nbsp;' : '<img class="mr-2" width="20" height="20" src="assets/images/login.png" alt="">' ?>
                <?= (isset($_SESSION['nameClient'])) ? $_SESSION['nameClient'] : 'Login' ?>
            </a>
        

            
	      </div>
          <div class="social-media d-flex align-items-center ml-auto" style="margin-left: 0px !important">
       

            <?php
            if(isset($_SESSION['nameClient'])){

            ?> 
            
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bell"></i>&nbsp;<span class="badge badge-light">4</span>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button class="dropdown-item" type="button">Action</button>
                <button class="dropdown-item" type="button">Another action</button>
                <button class="dropdown-item" type="button">Something else here</button>
            </div>
            </div>
	      </div>

          <div class="social-media d-flex align-items-center ml-auto" style="margin-left: 8px !important">
          
            <a href="<?= base_url; ?>/LogoutClient" class="d-flex align-items-center register-btn mr-2" style="background: #d13535;">
               Logout
            </a>
            <?php
            }
            ?>
            
	      </div>
          <?php
            }else{
           ?>
            <a href="<?= base_url; ?>/LoginClient" class="d-flex align-items-center register-btn mr-2">
                <i class="fa fa-user"></i> &nbsp Login
            </a>

            <?php    
            }
          ?>


	  </nav>
        </div>
    </header>


</div>

