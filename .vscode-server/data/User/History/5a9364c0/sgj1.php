<?php

class ClientDashboard extends Controller {
    public function __construct()
    {
	    if($_SESSION['session_login_client'] == 'true') {
			header('location: '. base_url . '/Admin');
			exit;
		}
    }

    public function index()
    {
        $data['title'] = 'Sipenda.';
        $data['aku']  = "aku";
        $this->view('views/partials/header',$data);
        $this->view('views/client/dashboard-client',$data);
        $this->view('views/partials/footer');

    }
}