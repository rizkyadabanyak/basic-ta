<?php
require '../contents/assets/Carbon/autoload.php';

use Carbon\Carbon;
use Carbon\CarbonInterval;

class AdminOrmawa extends Controller {
	public function __construct()
	{
		if($_SESSION['session_login'] != 'true') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/LoginAdmin');
			exit;
		}
	}

	public function index()
	{
	    $data['title'] = 'Sipenda.';
	    $data['aku']  = "aku";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);
		
		if($_SESSION['levelAdmin'] == '1'){
			$sql = "SELECT * FROM ORMAWA";
		}else{
			$email = $_SESSION['emailOrmawa'] ;
			$sql = "SELECT * FROM ORMAWA WHERE EMAIL='$email'";
		}

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['datas'] = $hasil;

	    $this->view('views/partials-admin/header',$data);
	    $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/admin/ormawa/v_index',$data);
	    $this->view('views/partials-admin/footer');
	}

	//ini buat view create
	public function create(){

		$data['title'] = 'Sipenda.';
	    $data['aku']  = "aku";
		$data['Name'] = $_SESSION['name'];
		$data['cek_form'] = 'create' ;

	    $this->view('views/partials-admin/header',$data);
	    $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/admin/ormawa/v_form',$data);
	    $this->view('views/partials-admin/footer');
	}
	//ini buat action create
	public function store(){

		// $dt = Carbon::now();

		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);


		$sql = "INSERT INTO ORMAWA (ID_ORMAWA,NAMA,EMAIL,PASSWD,VALID) VALUES(SEQ_ORMAWA.nextval, :v1, :v2,:v3,:v4)";


		$data = array(
			':v1' => $_POST['nama'],
			':v2' => $_POST['email'],
			':v3' => md5($_POST['password']),
			':v4' => '0', 
		);

		$hasil = query_insert($con, $sql, $data);

		Flasher::setMessage('Berhasil','ditambah','success');

		header('location: '. base_url . '/AdminOrmawa');
	}

	//ini buat view edit
	public function edit($id){	

		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "SELECT * FROM PENGUMUMAN WHERE ID_PENGUMUMAN='$id'";
		// $data = array(':v1' => $id);
	
		$hasil = oci_parse($con, $sql);
		
		$data['news'] = $hasil;
		$data['cek_form'] = 'edit';
		
		$this->view('views/partials-admin/header',$data);
	    $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/admin/news/v_form',$data);
	    $this->view('views/partials-admin/footer');
		// var_dump($baris);
	}
	
	//ini buat action edit
	public function update($id){
		
		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);
		
		$sqlData = "SELECT * FROM PENGUMUMAN WHERE ID_PENGUMUMAN='$id'";
	
		$getData = oci_parse($con, $sqlData);

		oci_execute($getData) or die(oci_error());				
	    $hasilGetData = oci_fetch_object($getData);


		if($_FILES["image"]["name"]){
			$temp = explode(".", $_FILES["image"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			$filepath = "../contents/uploads/" . $newfilename;
		
			if(move_uploaded_file($_FILES["image"]["tmp_name"], $filepath)){

			}else{
				echo "gagl upload";
			}
		}else{
			$newfilename = $hasilGetData->IMG;
		}
	
		$sql = "UPDATE PENGUMUMAN SET JUDUL=:v2,DSC=:v3,DSC_BANNER=:v4,IMG=:v5 WHERE ID_PENGUMUMAN=:v1";


		$data = array(
			':v1' => $id,
			':v2' => $_POST['judul'],
			':v3' => $_POST['dsc'],
			':v4' => strip_tags($_POST['dsc']), 
			':V5' => $newfilename
		);

		$hasil = query_update($con, $sql, $data);
		Flasher::setMessage('Berhasil','update','success');

		header('location: '. base_url . '/AdminOrmawa');

	}

	//ini buat action delete
	public function destroy($id)
	{
		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "DELETE PENGUMUMAN WHERE ID_PENGUMUMAN=:v1";
		$data = array(':v1' => $id);
		$hasil = query_delete($con, $sql, $data);
		
		Flasher::setMessage('Berhasil','dihapus','success');

		header('location: '. base_url . '/AdminOrmawa');
		exit;	

    
    }

	public function validation($id){
		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$sql = "SELECT * FROM ORMAWA WHERE ID_ORMAWA='$id'";
		// $data = array(':v1' => $id);
		$hasil = oci_parse($con, $sql);

		$sqlJurusan = "SELECT * FROM ORMAWA";
		
		$data['news'] = $hasil;
		$data['cek_form'] = 'edit';
		
		$this->view('views/partials-admin/header',$data);
	    $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/admin/ormawa/v_formEdit',$data);
	    $this->view('views/partials-admin/footer');
	}

	public function logo($img){
		$temp = explode(".", $img["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$filepath = "../contents/uploads/" . $newfilename;
	
		if(move_uploaded_file($img["tmp_name"], $filepath)){
			return $newfilename;
		}else{
			echo "gagl upload";
		}
		return; 
	}
	public function kabinet($img){
		$temp = explode(".", $img["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$filepath = "../contents/uploads/" . $newfilename;
	
		if(move_uploaded_file($img["tmp_name"], $filepath)){
			return $newfilename;
		}else{
			echo "gagl upload";
		}
		return; 
	}

	public function validationForm($id){
		logo
	
		$db_user = "PA0039";
		$db_pass = "399281";
		$con = konekDb($db_user, $db_pass);

		$logo = $this->logo($_FILES["logo_ormawa"]);
		$kabinet = $this->kabinet($_FILES["logo_kabinet"]);

		if($_POST['cekOrmawa'] == 'hima'){

			

		}else{

			$sql = "UPDATE ORMAWA SET NAMA=:v2,SINGKATAN=:v3,WEBSITE=:v4,UKM=:v5,LOGO=:v6,IMG_KABINET=:v7,VALID=:v8,CEK=:v9 WHERE ID_ORMAWA=:v1";

			$data = array(
				':v1' => $id,
				':v2' => $_POST['nama'],
				':v3' => $_POST['singkatan'],
				':v4' => $_POST['website'],
				':v5' => $_POST['namaUkm'],
				':v6' => $logo,
				':v7' => $kabinet,
				':v8' => '1', 
				':V9' => $_POST['cekOrmawa'],
			);
			$hasil = query_update($con, $sql, $data);

			var_dump($hasil);
			
		}
		Flasher::setMessage('Berhasil','update','success');
		header('location: '. base_url . '/AdminOrmawa');

	}

}