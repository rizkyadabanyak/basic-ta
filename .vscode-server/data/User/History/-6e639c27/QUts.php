    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <div class="row">
    <div class="col-sm-12">
      <?php
        Flasher::Message();
      ?>
    </div>
  </div>
                <h2 class="section-title">Ormawa</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                                    if($_SESSION['levelAdmin']=='1'){
                                                    ?>  
                                                        <a href="<?= base_url; ?>/AdminOrmawa/destroy/<?=$rows->ID_ORMAWA ?>" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>

                                                    <?php
                                                    }else{
                                                    ?> 
                                                    <?php
                                                        if($rows->VALID == '0'){
                                                    ?>  
                                                     
                                                        <a href="<?= base_url; ?>/AdminOrmawa/validation/<?=$rows->ID_ORMAWA ?>" class="btn btn-warning text-white">Validation</a>
                                                    <?php
                                                        }
                                                    ?>
                                                        <!-- end if -->
                                                    <?php
                                                    }
                                                    ?>
                                                    <!-- end if -->
                                <a href="<?= base_url; ?>/AdminOrmawa/create" class="btn btn-success text-white">create</a>
                                <br><br>
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped1" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Singkatan</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Jurusan</th>
                                                <th>Logo</th>
                                                <th>IMG_KABINET</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                oci_execute($data['datas']);
                                                while($rows=oci_fetch_object($data['datas'])){
                                            ?>
                                            <tr>
                                                <td><?php echo $rows->NAMA; ?></td>
                                                <td><?php echo $rows->SINGKATAN; ?></td>
                                                <td><?php echo $rows->EMAIL; ?></td>
                                                <td><?php echo $rows->WEBSITE; ?></td>
                                                <td><?php echo $rows->UKM; ?></td>
                                                <td><img src="<?= base_url.'/uploads/'.$rows->LOGO ?>" width="50"></td>
                                                <td><img src="<?= base_url.'/uploads/'.$rows->IMG_KABINET ?>" width="50"></td>

                                                <td>
                                                    <?php
                                                        if($rows->VALID == '1' || $_SESSION['levelAdmin']=='1'){
                                                    ?>  
                                                        <a href="<?= base_url; ?>/AdminOrmawa/edit/<?= $rows->ID_ORMAWA ?>" class="btn btn-primary text-white">Edit</a>
                                                    <?php
                                                        }
                                                    ?>
                                                    <!-- end if -->

                                                    <?php
                                                    if($_SESSION['levelAdmin']=='1'){
                                                    ?>  
                                                        <a href="<?= base_url; ?>/AdminOrmawa/destroy/<?=$rows->ID_ORMAWA ?>" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>

                                                    <?php
                                                    }else{
                                                    ?> 
                                                    <?php
                                                        if($rows->VALID == '0'){
                                                    ?>  
                                                     
                                                        <a href="<?= base_url; ?>/AdminOrmawa/validation/<?=$rows->ID_ORMAWA ?>" class="btn btn-warning text-white">Validation</a>
                                                    <?php
                                                        }
                                                    ?>
                                                        <!-- end if -->
                                                    <?php
                                                    }
                                                    ?>
                                                    <!-- end if -->

                                                
                                                </td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    
 <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>