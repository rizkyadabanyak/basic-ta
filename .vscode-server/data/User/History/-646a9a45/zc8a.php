<?php
include_once "../includes/func.inc.php";

class Controller{
		
	public function __construct()
    {
	    if($_SESSION['session_login_client'] != 'true') {
			header('location: '. base_url . '/LoginClient');
			exit;
		}
    }
	
	public function view($view, $data = [])
	{
		require_once '../contents/' . $view . '.php';
	}

//	public function model($model)
//	{
//		require_once '../admin/models/' . $model . '.php';
//		return new $model;
//	}
}