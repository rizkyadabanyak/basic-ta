<?php
include_once "../includes/func.inc.php";

class Controller{
		
	public function __construct()
    {
	    $_SESSION['session_login_client'] = 'false';
    }

	public function view($view, $data = [])
	{
		require_once '../contents/' . $view . '.php';
	}

//	public function model($model)
//	{
//		require_once '../admin/models/' . $model . '.php';
//		return new $model;
//	}
}